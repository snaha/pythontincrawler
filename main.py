#!/usr/bin/env python
import sys
import Textfile
import time
import UploadToS3
from crawler.crawl import Crawl
import config

if __name__ == '__main__':
    TIN = input("Enter 10 digit TIN number...\n")
    while not TIN or len(TIN) < 10:
        TIN = input("It's not a right one...Enter 10 digit TIN number...\n")
    url = r'http://www.tinxsys.com/TinxsysInternetWeb/dealerControllerServlet?tinNumber=' + TIN + \
          '&searchBy=TIN&backPage=searchByTin_Inter.jsp'
    print("Please wait... fetching data...")
    crawler = Crawl(url)
    res = crawler.parse_data(crawler.getresponse('table', {}))
    if not res or len(res) == 0:
        print("The TIN number is not valid!")
        sys.exit()
    file_name = config.file_base + str(int(time.time())) + ".txt"
    print("creating file " + file_name.split("/")[1] + "...")
    file = Textfile.createfile(file_name)
    if file is None:
        print("Couldn't create the file with name : " + file_name.split("/")[1])
        sys.exit()
    Textfile.writedata(file, res)
    status = UploadToS3.uploadfiletos3('python6', file_name.split("/")[1])
    if status:
        print("The file is successfully uploaded to S3 : URL : " + status)
