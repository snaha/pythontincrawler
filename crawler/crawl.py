import requests
from bs4 import BeautifulSoup
import re


class Crawl:
    url = ''

    def __init__(self, url):
        self.url = url

    def getresponse(self, selector, criteria):
        source_code = requests.get(self.url)
        plain_text = source_code.text
        soup = BeautifulSoup(plain_text, 'lxml')
        objects = soup.findAll(selector, criteria)
        return objects

    def parse_data(self, objects):
        results = []
        for table in objects:
            rows = table.findAll('tr')
            if len(rows) <= 1:
                continue  # this is not the correct table
            i = 1
            text_table = []
            for tr in rows:
                if i == 1:
                    i += 1
                    continue  # excluding the heading
                row = []
                cols = tr.findAll('td')
                for td in cols:
                    if td is None:
                        continue
                    if td.find('div') is not None:
                        text = str(td.find('div').text).strip()  # for td with div
                    else:
                        text = str(td.text).strip()  # td without div
                    if text is None or text is 'None':
                        continue
                    row.append(re.sub(r'\s\s+', ' ', text))
                text_table.append(row)
                i += 1
            # if not text_table:
            results.append(text_table)
        if len(results) == 1:  # reformatting
            results = results[0]
        return results
