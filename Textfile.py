import os


def createfile(title):
    try:
        if not os.path.exists(os.path.dirname(title)):
            os.makedirs(os.path.dirname(title))
        fw = open(title, "w")
    except FileExistsError:
        try:
            fw = open(title, "r")
        except FileNotFoundError:
            fw = None
        except:
            fw = None
    except:
        fw = None
    return fw


def writedata(file, data):
    if not file:
        return False
    for row in data:
        file.write(str(row[0]) + " : " + str(row[1]) + "\n")
    file.close()
    return True
