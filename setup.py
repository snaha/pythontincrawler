import sys
from setuptools import setup

setup(
    name="CrawlForTIN",  # what you want to call the archive/egg
    version="0.1",
    packages=["crawler"],  # top-level python modules you can import like
    #   'import foo'
    dependency_links=[],  # custom links to a specific project
    install_requires=['requests', 'bs4', 'lxml'],
    extras_require={},  # optional features that other packages can require
    #   like 'helloworld[foo]'
    package_data={},
    author="Shubhabrata Naha",
    author_email="shubhabratanaha@yahoo.com",
    description="Crawler for TIN verification in Python",
    license="",
    keywords="python",
    url="http://github.com/snaha/python-crawl-and-s3",
    entry_points={
        "console_scripts": [  # command-line executables to expose
            "main_in_python = main",
        ],
        "gui_scripts": []  # GUI executables (creates pyw on Windows)
    }
)
