import boto
import boto.s3
from boto.s3.key import Key
import config


def gets3bucket(bucket_name=''):
    aws_access_key = config.AWS_KEY['ACCESS_KEY']
    aws_secret_key = config.AWS_KEY['SECRET_KEY']

    conn = boto.connect_s3(aws_access_key,
                           aws_secret_key)
    try:
        bucket = conn.get_bucket(bucket_name)
    except boto.exception.S3ResponseError:
        try:
            bucket = conn.create_bucket(bucket_name, location=boto.s3.connection.Location.DEFAULT)
        except boto.exception.S3PermissionsError:
            print("Not enough permission...")
            return None
        except boto.exception.S3CreateError:
            print("Couldn't create S3 Bucket...")
            return None
    return bucket


def uploadfiletos3(bucket_name, file_name):
    bucket = gets3bucket(bucket_name)
    print('Uploading %s to Amazon S3 bucket %s' % (file_name, bucket_name))
    byts = 0
    try:
        k = Key(bucket)
        k.key = file_name
        byts = k.set_contents_from_filename(config.file_base + file_name, encrypt_key=False)
        k.set_acl('public-read')
        url = k.generate_url(expires_in=3600*24*365)
        return url
    except boto.exception.S3ResponseError:
        print("No response from S3..please try after sometime...")
    except boto.exception.S3PermissionsError:
        print("You don't have access to S3")
    except:
        print("Something went wrong while uploading to S3 ")
        return False
    if byts == 0:
        print("Something went wrong while uploading to S3")
        return False
    return False
